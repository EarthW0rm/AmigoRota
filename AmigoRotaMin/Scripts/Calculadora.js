﻿var App = App || {};

function initObjects() {

    this.GoogleMap = null;

    this.DirectionsResult = null;

    this.Paradas = [];

    this.Componets = {
        txtLatLngPartida: $('#txtLatLngPartida')
        , txtLatLngChegada: $('#txtLatLngChegada')
        , txtKMTrecho: $('#txtKMTrecho')
        , txtDataPartida: $('#txtDataPartida')
        , txtTempoAbastecimento: $('#txtTempoAbastecimento')
        , txtTempoAlmoco: $('#txtTempoAlmoco')
        , txtLatLngParada: $('#txtLatLngParada')
        , txtTempoParada: $('#txtTempoParada')
        , btnAddParada: $('#btnAddParada')
        , btnCalcular: $('#btnCalcular')
        , lstParadas: $('#lstParadas')
        , mapPlace: $('#mapViewPlace')
        , DataPartida: new Date(2016, 03, 09, 7, 0, 0, 0)
    };

    this.PlayRoutePath = [];
    this.PlayRoutePathIndex = 0;
    this.PlayRoutePathMarker = null;

    this.PlayRoute = function () {
        debugger;

        for (var i = 0; i < this.DirectionsResult.routes[0].legs.length; i++) {
            var itemLeg = this.DirectionsResult.routes[0].legs[i];
            for (var j = 0; j < itemLeg.steps.length; j++) {
                var itemStep = itemLeg.steps[j];
                for (var k = 0; k < itemStep.path.length; k++) {
                    this.PlayRoutePath.push(itemStep.path[k]);
                }
            }
        }

        App.PlayRoutePathMarker = new google.maps.Marker({
            position: App.PlayRoutePath[App.PlayRoutePathIndex],
            icon: 'Content/motorbike_red.png'
        });
        App.PlayRoutePathMarker.setMap(App.GoogleMap);
        App.PlayRoutePathIndex += 1;

        setInterval(function () {
            if (App.PlayRoutePathIndex < App.PlayRoutePath.length) {
                App.PlayRoutePathMarker.setPosition(App.PlayRoutePath[App.PlayRoutePathIndex]);
                App.PlayRoutePathIndex += 1;
            } else {
                App.PlayRoutePathMarker.setMap(null);
                clearInterval();
            }
        }, 1);
    }
    this.PathLocations = [];

    this.GasMarkers = [];

    this.PathLocationsPages = [];

    this.Delegates = this.Delegates || {};

    this.Delegates.incluirParada = function () {

        var latLngPatada = App.Componets.txtLatLngParada.val();

        var latlngExpression = new RegExp(/([ ]*)?([-+]?[0-9]+([.]{0,1})?([0-9]+)?)([ ]*)?([,])([ ]*)?([-+]?[0-9]+([.]{0,1})?([0-9]+)?)([ ]*)?/g);

        if (latlngExpression.test(latLngPatada)) {

            for (var i = 0; i < App.Paradas.length; i++) {
                if (App.Paradas[i].location.lat == parseFloat(latLngPatada.split(",")[0]) && App.Paradas[i].location.lng == parseFloat(latLngPatada.split(",")[1])) {
                    window.alert('Ponto de parada já incluido!');
                    return;
                }
            }

            App.Paradas.push({
                location: {
                    lat: parseFloat(latLngPatada.split(",")[0]),
                    lng: parseFloat(latLngPatada.split(",")[1])
                }
                , Tempo: App.Componets.txtTempoParada.val()
            });

            App.Componets.txtLatLngParada.val('');

        } else {
            window.alert('Latitude, Longitude da parada é inválida');
        }

        App.Componets.lstParadas.html('');

        for (var i = 0; i < App.Paradas.length; i++) {
            App.Componets.lstParadas.append('<li><b>Lat:</b> ' + App.Paradas[i].location.lat + ', <b>Lng:</b> ' + App.Paradas[i].location.lng + ' - <b>Tempo de parada:</b> ' + App.Paradas[i].Tempo + '</li>')
        }
    };

    this.Delegates.calcularRota = function () {
        if (App.ValidarCampos()) {
            App.initMap();

            App.LoadingModal.showPleaseWait();

            App.Componets.TempoAbastecimento = (parseInt(App.Componets.txtTempoAbastecimento.val().split(':')[0]) * 60) + parseInt(App.Componets.txtTempoAbastecimento.val().split(':')[1]);
            App.Componets.TempoAlmoco = (parseInt(App.Componets.txtTempoAlmoco.val().split(':')[0]) * 60) + parseInt(App.Componets.txtTempoAlmoco.val().split(':')[1]);


            var listaDestinos = App.AgruparDestinos();

            var directionsService = new google.maps.DirectionsService();

            var reqDirection = {
                origin: listaDestinos[0].location
                    , destination: listaDestinos[listaDestinos.length - 1].location
                    , waypoints: listaDestinos.slice(1, listaDestinos.length - 1)
                    , optimizeWaypoints: false
                    , travelMode: google.maps.DirectionsTravelMode.DRIVING
                    , transitOptions: {
                        departureTime: App.Componets.DataPartida
                    }
                    , drivingOptions: {
                        departureTime: App.Componets.DataPartida
                        , trafficModel: "pessimistic" // google.maps.TrafficModel.OPTIMISTIC, google.maps.TrafficModel.BEST_GUESS

                    }
                    , unitSystem: google.maps.UnitSystem.METRIC
            };

            directionsService.route(reqDirection, function (result, status) {
                if (status == google.maps.DirectionsStatus.OK) {
                    App.DirectionsResult = result;
                    var directionsDisplay = new google.maps.DirectionsRenderer();

                    directionsDisplay.setMap(App.GoogleMap);
                    directionsDisplay.setOptions({ suppressMarkers: true });
                    directionsDisplay.setDirections(result);

                    var kmsPercorrido = 0.0;
                    var kmsPercorridoTotal = 0.0;
                    var secDecorridos = 0;
                    var secDecorridosTotal = 0;
                    var dataAtual = new Date(App.Componets.DataPartida);
                    var kmsParada = parseInt(App.Componets.txtKMTrecho.val());

                    for (var i = 0; i < result.routes[0].legs.length; i++) {
                        var itemLeg = result.routes[0].legs[i];

                        for (var j = 0; j < itemLeg.steps.length; j++) {
                            var itemStep = itemLeg.steps[j];

                            secDecorridos += itemStep.duration.value;
                            secDecorridosTotal += itemStep.duration.value;
                            kmsPercorrido += (itemStep.distance.value / 1000);
                            kmsPercorridoTotal += (itemStep.distance.value / 1000);
                            dataAtual.setSeconds(dataAtual.getSeconds() + itemStep.duration.value);

                            //QUANDO ATINGIR O MAXIMO DO TRECHO
                            if (kmsPercorrido >= kmsParada) {

                                var dataSaida = new Date(dataAtual);
                                dataSaida.setMinutes(dataSaida.getMinutes() + App.Componets.TempoAbastecimento);

                                var dataTempo = new Date(1982, 5, 15, 0, 0, 0, 0);
                                dataTempo.setSeconds(secDecorridos);

                                var _marker = new google.maps.Marker({
                                    position: itemStep.end_location,
                                    title: dataAtual.toString(),
                                    icon: 'Content/fillingstation.png',
                                    infowindow: new google.maps.InfoWindow({
                                        content: '<div id="content"><div id="bodyContent"><ul>'
                                            + '<li><b>Latitude Longitude:</b>' + itemStep.end_location.lat() + " , " + itemStep.end_location.lng() + '</li>'
                                            + '<li><b>Quilometragem:</b>' + kmsPercorrido + '</li>' + '<li><b>Quilometragem Total:</b>' + kmsPercorridoTotal + '</li>'
                                            + '<li><b>Tempo a partir do ultimo ponto:</b> horas ' + dataTempo.getHours() + ', minutos ' + dataTempo.getMinutes() + '</li>'
                                            + '<li><b>Horario Chegada:</b>' + dataAtual.toString() + '</li>'
                                            + '<li><b>Horario Saida:</b>' + dataSaida.toString() + '</li>'
                                            + '</ul></div></div>'
                                    })
                                });

                                dataAtual.setMinutes(dataAtual.getMinutes() + App.Componets.TempoAbastecimento);

                                _marker.addListener('click', function (e) {
                                    this.infowindow.open(App.GoogleMap, this);
                                });
                                _marker.setMap(App.GoogleMap);

                                App.GasMarkers.push(_marker);

                                secDecorridos = 0;
                                kmsPercorrido = 0;
                            }
                        }

                        if (App.Paradas.length > 0) {
                            var parada = App.Paradas[i];
                            if (parada != undefined) {
                                var tempoParada = (parseInt(parada.Tempo.split(':')[0]) * 60) + parseInt(parada.Tempo.split(':')[1])

                                var dataSaida = new Date(dataAtual);
                                dataSaida.setMinutes(dataSaida.getMinutes() + tempoParada);

                                var _marker = new google.maps.Marker({
                                    position: itemLeg.end_location,
                                    title: dataAtual.toString(),
                                    icon: 'Content/photo.png',
                                    infowindow: new google.maps.InfoWindow({
                                        content: '<div id="content"><div id="bodyContent"><ul>'
                                            + '<li><b>Latitude Longitude:</b>' + itemLeg.end_location.lat() + " , " + itemLeg.end_location.lng() + '</li>'
                                            + '<li><b>Horario Chegada:</b>' + dataAtual.toString() + '</li>'
                                            + '<li><b>Horario Saida:</b>' + dataSaida.toString() + '</li>'
                                            + '</ul></div></div>'
                                    })
                                });

                                dataAtual.setMinutes(dataAtual.getMinutes() + tempoParada);

                                _marker.addListener('click', function (e) {
                                    this.infowindow.open(App.GoogleMap, this);
                                });
                                _marker.setMap(App.GoogleMap);

                                App.GasMarkers.push(_marker);
                            }
                        }

                        if (i == 0) {
                            var _marker = new google.maps.Marker({
                                position: itemLeg.start_location,
                                title: App.Componets.DataPartida.toString(),
                                icon: 'Content/motorbike.png',
                                infowindow: new google.maps.InfoWindow({
                                    content: '<div id="content"><div id="bodyContent"><ul>'
                                        + '<li><b>Latitude Longitude:</b>' + itemLeg.start_location.lat() + " , " + itemLeg.start_location.lng() + '</li>'
                                        + '<li><b>Horario Saida:</b>' + App.Componets.DataPartida.toString() + '</li>'
                                        + '</ul></div></div>'
                                })
                            });

                            _marker.addListener('click', function (e) {
                                this.infowindow.open(App.GoogleMap, this);
                            });
                            _marker.setMap(App.GoogleMap);

                            App.GasMarkers.push(_marker);
                        }

                        if (i == result.routes[0].legs.length - 1) {
                            var _marker2 = new google.maps.Marker({
                                position: itemLeg.end_location,
                                title: dataAtual.toString(),
                                icon: 'Content/finish.png',
                                infowindow: new google.maps.InfoWindow({
                                    content: '<div id="content"><div id="bodyContent"><ul>'
                                        + '<li><b>Latitude Longitude:</b>' + itemLeg.end_location.lat() + " , " + itemLeg.end_location.lng() + '</li>'
                                        + '<li><b>Horario Chegada:</b>' + dataAtual.toString() + '</li>'
                                        + '</ul></div></div>'
                                })
                            });

                            _marker2.addListener('click', function (e) {
                                this.infowindow.open(App.GoogleMap, this);
                            });
                            _marker2.setMap(App.GoogleMap);

                            App.GasMarkers.push(_marker2);
                        }

                    }

                    App.LoadingModal.hidePleaseWait();
                    App.PlayRoute();
                }
            });

        }
    };

    this.Sleep = function () {
        var e = new Date().getTime() + (10 * 1000);
        while (new Date().getTime() <= e) { }
    }

    this.AgruparDestinos = function () {
        var agrupados = [];

        var chegada = this.Componets.txtLatLngChegada.val();
        var partida = this.Componets.txtLatLngPartida.val();

        agrupados.push({
            location: {
                lat: parseFloat(partida.split(",")[0]),
                lng: parseFloat(partida.split(",")[1])
            }
        });

        for (var i = 0; i < App.Paradas.length; i++) {
            agrupados.push({ location: App.Paradas[i].location });
        }
        agrupados.push({
            location: {
                lat: parseFloat(chegada.split(",")[0]),
                lng: parseFloat(chegada.split(",")[1])
            }
        });

        return agrupados;
    }

    this.LoadingModal = {
        showPleaseWait: function () {
            $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
        },
        hidePleaseWait: function () {
            $('#modalLoading').modal('hide');
        },
    }

    this.ValidarCampos = function () {
        var latlngExpression1 = new RegExp(/([ ]*)?([-+]?[0-9]+([.]{0,1})?([0-9]+)?)([ ]*)?([,])([ ]*)?([-+]?[0-9]+([.]{0,1})?([0-9]+)?)([ ]*)?/g);
        var latlngExpression2 = new RegExp(/([ ]*)?([-+]?[0-9]+([.]{0,1})?([0-9]+)?)([ ]*)?([,])([ ]*)?([-+]?[0-9]+([.]{0,1})?([0-9]+)?)([ ]*)?/g);

        var chegada = this.Componets.txtLatLngChegada.val();
        var partida = this.Componets.txtLatLngPartida.val();

        if (!latlngExpression1.test(chegada) || !latlngExpression2.test(partida)) {
            window.alert("Lat-Lng de Chegada ou Partida inválida!");
            return false;
        }

        return true;
    }

    this.bindEvents = function () {
        this.Componets.btnAddParada.off().on('click', this.Delegates.incluirParada);
        this.Componets.btnCalcular.off().on('click', this.Delegates.calcularRota);

        this.Componets.txtDataPartida.datetimepicker({
            locale: 'pt-br'
        }).on('dp.change', function (ev, date) {
            App.Componets.DataPartida = new Date(ev.date);
            App.Componets.DataPartida.setSeconds(0);
        });
    };

    this.initMap = function () {
        var mapHeight = $(document).height() - 119;
        this.Componets.mapPlace.css('height', mapHeight + 'px');

        this.GoogleMap = new google.maps.Map(this.Componets.mapPlace[0], {
            center: { lat: -23.608336, lng: -46.646996 },
            zoom: 8
        });

        this.bindEvents();
    };

    return this;
}

$(document).ready(function () {
    $('[data-toggle="tooltip"]').tooltip();

    $('.settimepicker').datetimepicker({
        format: 'H:mm'
    });

    App = new initObjects();
    App.initMap();
});