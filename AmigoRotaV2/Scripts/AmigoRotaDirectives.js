﻿var app = angular.module("AmigoRotaWebApp");

app.directive("numericField", function () {

    return {
        restrict: 'A'
        , link: function (scope, element, attrs) {
            $(element[0]).inputmask("[0-9]+");
        }
    };
});

app.directive("dateTimePicker", function () {

    return {
        restrict: 'A'
        , link: function (scope, element, attrs) {
            var dtpConfig = {
                locale: 'pt-br'
            };

            if (attrs.dateTimePickerFormat && attrs.dateTimePickerFormat != "") { }
                dtpConfig.format = attrs.dateTimePickerFormat;

                $(element[0]).datetimepicker(dtpConfig)
                    .on('dp.change', function (ev, date) {
                        scope.$apply(function () {
                            var selectedText = element.val();
                            var ngModelArray = attrs.ngModel.split('.');

                            var treeScopeFinder = scope;
                            for (var i = 0; i < ngModelArray.length - 1; i++) {
                                treeScopeFinder = treeScopeFinder[ngModelArray[i]];
                            }

                            treeScopeFinder[ngModelArray[ngModelArray.length - 1]] = {
                                value: selectedText,
                                date:  new Date(ev.date),
                                toString: function () {
                                    return this.value;
                                }
                            }

                        });
                    });
        }
    };
});

app.directive("amigoRotaAppMenu", function () {

    return {
        restrict: 'E'
        , templateUrl: '/Views/Templates/amigoRotaAppMenu-template.html'
        , controller: ["$scope", "$location", "$rootScope", function ($scope, $location, $rootScope) {
            $scope.MenuItensConfiguration = $rootScope.Configuration.AppFunctions;

            $scope.ChangeFunction = function (selectedItem) {
                $rootScope.ChangeLocation(selectedItem);
            };
        }]
    };
});

app.directive("amigoRotaAppBreadCrumb", function () {

    return {
        restrict: 'E'
        , templateUrl: '/Views/Templates/amigoRotaAppBreadCrumb-template.html'
        , controller: ["$scope", "$rootScope", function ($scope, $rootScope) {
            $scope.MenuItensConfiguration = $rootScope.Configuration.AppFunctions;

            $scope.SelectedFunction = $scope.MenuItensConfiguration.MenuItens[$scope.MenuItensConfiguration.selectedIndex];

            $scope.$watch('MenuItensConfiguration', function () {
                $scope.SelectedFunction = $scope.MenuItensConfiguration.MenuItens[$scope.MenuItensConfiguration.selectedIndex];
            }, true);
        }]
    };
});

app.directive("amigoRotaAppMap", ["amigoRotaMapService", "$compile", function (amigoRotaMapService, $compile) {

    var currentUserMark = null;
    var currentUserInfoWindow = null;
    var currentGeocode = null;
    var currentUserFixedMapPoints = [];

    function TranslateStringLocation(strLocation) {
        var objRetorno = {
            position: null,
            address: null
        }

        var latlngExpression = new RegExp(/([ ]*)?([-+]?[0-9]+([.]{0,1})?([0-9]+)?)([ ]*)?([,])([ ]*)?([-+]?[0-9]+([.]{0,1})?([0-9]+)?)([ ]*)?/g);

        if (latlngExpression.test(strLocation)) {
            objRetorno.position = {
                lat: parseFloat(strLocation.split(",")[0]),
                lng: parseFloat(strLocation.split(",")[1])
            }
        } else {
            objRetorno.address = strLocation;
        }

        return objRetorno;
    }
        
    function UserSelectMapPosition(locationObject, scope) {
        if (currentUserMark != null) {
            currentUserMark.setMap(null);
            currentUserInfoWindow.setMap(null);
        }

        var GeoService = new google.maps.Geocoder;
        GeoService.geocode(locationObject, function (results, status) {
            if (status === google.maps.GeocoderStatus.OK) {
                if (results.length > 0) {

                    currentUserMark = new google.maps.Marker({
                        position: results[0].geometry.location,
                        draggable: true,
                        fillColor: "#FF8000",
                        icon: "/mapsPins/information-gray.png",
                        animation: google.maps.Animation.DROP
                    });

                    google.maps.event.addListener(currentUserMark, 'dragend', function (e) {
                        UserSelectMapPosition({ location: e.latLng }, scope);
                    });

                    google.maps.event.addListener(currentUserMark, 'click', function (e) {
                        currentUserInfoWindow.open(amigoRotaMapService.CurrentMap, this);
                    });

                    scope.selectedMapMark = currentUserMark;
                    scope.selectedGeoCode = currentGeocode = results;
                    scope.Detail = {
                        Title: "Novo ponto de parada"
                        , LatLngString: scope.selectedMapMark.position.lat() + " , " + scope.selectedMapMark.position.lng()
                        , AddressString: scope.selectedGeoCode[0].formatted_address
                        , IsNew: true
                    }

                    var infoScope = scope.$new();
                    infoScope.Detail = angular.copy(scope.Detail);
                    
                    var contentString = '<div><div ng-include="\'infoWindowUserClick\'"></div></div>';
                    var compiledHtml = $compile(contentString)(infoScope);

                    scope.$apply();

                    currentUserInfoWindow = new google.maps.InfoWindow({
                        content: compiledHtml[0]
                    });

                    currentUserMark.InfoWindow = currentUserInfoWindow

                    currentUserMark.setMap(amigoRotaMapService.CurrentMap);
                    
                    amigoRotaMapService.CurrentMap.panTo(currentUserMark.position);
                }
            }
        });        
    }
  
    return {
        restrict: 'E'
        , scope: { addPinMapMethod: "=addMethod", mapInit: "=mapInit", storedMapMarks: "=getStoredMapMarks" }
        , templateUrl: '/Views/Templates/amigoRotaAppMap-template.html'
        , link: function (scope, element, attrs) {

            amigoRotaMapService.CurrentMap = new google.maps.Map(element.find("#mapPlace")[0], {
                center: { lat: -23.552589, lng: -46.653395 },
                zoom: 8
            });

            amigoRotaMapService.CurrentMap.addListener('click', function (e) {
                UserSelectMapPosition({ location: e.latLng }, scope);
            });

            scope.mapInit();

        }
        , controller: ["$scope", "amigoRotaMapService", "amigoRotaService", "$compile", function ($scope, amigoRotaMapService, amigoRotaService, $compile) {
                    
            $scope.AddUserMapPoint = function () {
                if (currentUserMark != null) {
                    currentUserMark.setMap(null);
                    currentUserInfoWindow.setMap(null);
                }

                $scope.addPinMapMethod({
                    location: $scope.selectedGeoCode[0].geometry.location
                    , MapMark: angular.copy($scope.selectedMapMark)
                    , GeoCode: $scope.selectedGeoCode
                    , Detail: {
                        Title: ""
                        , LatLngString: $scope.selectedMapMark.position.lat() + " , " + $scope.selectedMapMark.position.lng()
                        , AddressString: $scope.selectedGeoCode[0].formatted_address
                        , IsNew: false
                    }
                });
            };
            
            $scope.FindLocation = function () {
                var currentSearch = $scope.StringLocation;
                var selectedPosition = TranslateStringLocation(currentSearch);

                if (selectedPosition.position != null) {
                    UserSelectMapPosition({ location: selectedPosition.position }, $scope);
                } else {
                    UserSelectMapPosition({ address: selectedPosition.address }, $scope);
                }

            }
        }]

    };
}]);

app.directive("amigoRotaConfigurationBar", function () {

    return {
        restrict: 'E'
        , templateUrl: '/Views/Templates/amigoRotaConfigurationBar-template.html'
        , controller: ["$scope", "$location", "$rootScope", function ($scope, $location, $rootScope) {
            $scope.RouteDetails = $rootScope.Configuration.RouteDetails;
        }]
    };
});