﻿var app = angular.module("AmigoRotaWebApp", ['ngRoute', 'ngAnimate','lvl.directives.dragdrop']);

//app.animation('.item-pousos');

app.config(['$routeProvider', '$locationProvider', 
function ($routeProvider, $locationProvider) {
    //$locationProvider.html5Mode(true);

    $routeProvider.
        when('/trajetos', {
            templateUrl: '/Views/Trajetos.cshtml'
            , caseInsensitiveMatch: true
        }).
        when('/pousos', {
            templateUrl: '/Views/Pousos.cshtml'
            , caseInsensitiveMatch: true
        }).
        when('/dashboard', {
            templateUrl: '/Views/Dashboard.cshtml'
            , caseInsensitiveMatch: true
        }).
        when('/passeios', {
            templateUrl: '/Views/Passeios.cshtml'
            , caseInsensitiveMatch: true
        }).
        when('/', {
            templateUrl: '/Views/Dashboard.cshtml'
            , caseInsensitiveMatch: true
        }).
        otherwise({
            redirectTo: '/'
        });
}])
.run(['$location', '$rootScope', '$timeout', function (location, $rootScope, $timeout) {
    
    $rootScope.Configuration = {
        AppFunctions: {
            selectedIndex: 0
            , MenuItens: [{
                route: "/dashboard"
                , functionTitle: "Dashboard"
                , functionIcon: "fa-motorcycle"
                , functionDescription: "NA"
            }
            , {
                route: "/trajetos"
                , functionTitle: "Trajetos"
                , functionIcon: "fa-map"
                , functionDescription: "NA"
            }
            , {
                route: "/pousos"
                , functionTitle: "Pousos e Hotéis"
                , functionIcon: "fa-hotel"
                , functionDescription: "NA"
            }, {
                route: "/passeios"
                , functionTitle: "Passeios e Paradas"
                , functionIcon: "fa-camera"
                , functionDescription: "NA"
            }]
        }, RouteDetails: {
			Checkin:null
			, Nights: "7"
			, SectorKms: "160"
			, VehicleConsume: "16"
			, GasCost: "4.5"
			, SectorRestTime: "35"
			, PathLunchTime: "75"
			, SectorHeavyTrafficAddTime: "240"
			, SectorModerateTrafficAddTime: "120"
		}
    };
	
	var dateNow = new Date();
	
	$rootScope.Configuration.RouteDetails.Checkin = {
		value: dateNow.toLocaleDateString(),
		date: dateNow,
		toString: function () {
			return this.value;
		}
	};
    
    $rootScope.ChangeLocation = function (AppFunction) {
        $rootScope.Configuration.AppFunctions.selectedIndex = $rootScope.Configuration.AppFunctions.MenuItens.indexOf(AppFunction);
        location.path(AppFunction.route);
    }

    //amigoRotaService.Configuration.AppFunctions
    for (var i = 0; i < $rootScope.Configuration.AppFunctions.MenuItens.length ; i++) {
        if ($rootScope.Configuration.AppFunctions.MenuItens[i].route.toLowerCase() == location.path().toLowerCase()) {
            $rootScope.Configuration.AppFunctions.selectedIndex = i;
        }
    }
	
	$timeout(function(){
		$.AdminLTE.controlSidebar.activate();
	}, 0);
	
    location.path(location.path());
}]);

app = angular.module("AmigoRotaWebApp");

app.service("amigoRotaService", function () {
   
    this.PousosHoteis = [];

    this.PasseiosParadas = [];

    this.StorePousosHoteisData = function () {
        return;

        var retArray = [];
        if (this.PousosHoteis.length > 0) {
            for (var ix = 0; ix < this.PousosHoteis.length; ix++) {
                var pouso = this.PousosHoteis[ix];

                var objTranlated = {
                    Detail: {
                        Title: pouso.Detail.Title
                            , LatLngString: pouso.MapMark.position.lat() + " , " + pouso.MapMark.position.lng()
                            , AddressString: pouso.GeoCode[0].formatted_address
                            , Checkin: { value: '', date: ''}
                            , Checkout: { value: '', date: '' }
                    }
                    , location: { lat: pouso.MapMark.position.lat(), lng: pouso.MapMark.position.lng() }
                    , image: pouso.MapMark.icon
                };

                if (pouso.Detail.Checkin && pouso.Detail.Checkin.value != "") {
                    objTranlated.Detail.Checkin = { value: pouso.Detail.Checkin.value, date: pouso.Detail.Checkin.date.toISOString() }
                }

                if (pouso.Detail.Checkout && pouso.Detail.Checkout.value != "") {
                    objTranlated.Detail.Checkout = { value: pouso.Detail.Checkout.value, date: pouso.Detail.Checkout.date.toISOString() }
                }


                objTranlated.GeoCode = angular.copy(pouso.GeoCode);

                retArray.push(objTranlated);
            }

            
        }

        $.jStorage.set('StoredPousos', retArray  );
    }

    this.GetStorePousosHoteisData = function () {
        return [];

        return $.jStorage.get('StoredPousos');
    }

    this.StorePasseiosParadasData = function () {
        return;

        var retArray = [];
        if (this.PasseiosParadas.length > 0) {
            for (var ix = 0; ix < this.PasseiosParadas.length; ix++) {
                var passeio = this.PasseiosParadas[ix];

                var objTranlated = {
                    Detail: {
                        Title: passeio.Detail.Title
                            , LatLngString: passeio.MapMark.position.lat() + " , " + passeio.MapMark.position.lng()
                            , AddressString: passeio.GeoCode[0].formatted_address
                            , Checkin: { value: '', date: '' }
                            , Checkout: { value: '', date: '' }
                    }
                    , location: { lat: passeio.MapMark.position.lat(), lng: passeio.MapMark.position.lng() }
                    , image: passeio.MapMark.icon
                };

                if (passeio.Detail.Checkin && passeio.Detail.Checkin.value != "") {
                    objTranlated.Detail.Checkin = { value: passeio.Detail.Checkin.value, date: passeio.Detail.Checkin.date.toISOString() }
                }

                if (passeio.Detail.Checkout && passeio.Detail.Checkout.value != "") {
                    objTranlated.Detail.Checkout = { value: passeio.Detail.Checkout.value, date: passeio.Detail.Checkout.date.toISOString() }
                }


                objTranlated.GeoCode = angular.copy(passeio.GeoCode);

                retArray.push(objTranlated);
            }


        }

        $.jStorage.set('StoredPasseios', retArray);
    }


    this.GetStorePasseiosParadasData = function () {
        return [];

        return $.jStorage.get('StoredPasseios');
    }
});

app.service("amigoRotaMapService", function () {

    this.CurrentMap = null;

    this.FixedMapPoints = [];

    this.UpdateFixedInfoWindow = function (locationObject, scope, compile) {
        var index = this.FixedMapPoints.indexOf(locationObject);
        if (index >= 0) {
            var infoScope = scope.$new();

            infoScope.Detail = angular.copy(locationObject.Detail);

            var contentString = '<div><div ng-include="\'infoWindowUserClick\'"></div></div>';

            var compiledHtml = compile(contentString)(infoScope);

            locationObject.MapMark.InfoWindow = new google.maps.InfoWindow();

            locationObject.MapMark.InfoWindow.setContent(compiledHtml[0]);

            google.maps.event.clearListeners(locationObject.MapMark, 'click');

            google.maps.event.addListener(locationObject.MapMark, 'click', function (e) {
                this.InfoWindow.open(this.map, this);
            });
        }
    }

    this.AddFixedMapPoints = function (locationObject, scope, compile) {
    
        this.FixedMapPoints.push(locationObject);

        var infoScope = scope.$new();

        infoScope.Detail = angular.copy(locationObject.Detail);        

        var contentString = '<div><div ng-include="\'infoWindowUserClick\'"></div></div>';
        var compiledHtml = compile(contentString)(infoScope);
        

        locationObject.MapMark.InfoWindow = new google.maps.InfoWindow();        
        locationObject.MapMark.InfoWindow.setContent(compiledHtml[0]);

        google.maps.event.clearListeners(locationObject.MapMark, 'click');
        
        locationObject.MapMark.InfoWindow.close();
        locationObject.MapMark.setMap(this.CurrentMap);

        google.maps.event.addListener(locationObject.MapMark, 'click', function (e) {
            this.InfoWindow.open(this.map, this);
        });
    }

    this.RestoreFixedMapPoint = function (restoredObject, scope, compile) {

        var contructedObject = {
            location: restoredObject.GeoCode[0].geometry.location
            , MapMark: null
            , GeoCode: restoredObject.GeoCode
            , Detail: {
                Title: restoredObject.Detail.Title
                , LatLngString: restoredObject.Detail.LatLngString
                , AddressString: restoredObject.Detail.AddressString
                , IsNew: false
            }
        }

        if (restoredObject.Detail.Checkin && restoredObject.Detail.Checkin.date != "") {

            contructedObject.Detail.Checkin = {
                value: restoredObject.Detail.Checkin.value,
                date: new Date(restoredObject.Detail.Checkin.date),
                toString: function () {
                    return this.value;
                }
            };
        }

        if (restoredObject.Detail.Checkout && restoredObject.Detail.Checkout.date != "") {

            contructedObject.Detail.Checkout = {
                value: restoredObject.Detail.Checkout.value,
                date: new Date(restoredObject.Detail.Checkout.date),
                toString: function () {
                    return this.value;
                }
            };
        }

        contructedObject.MapMark = new google.maps.Marker({
            position: contructedObject.GeoCode[0].geometry.location,
            draggable: true,
            icon: contructedObject.image,
            animation: google.maps.Animation.DROP
        });

        

        var infoScope = scope.$new();
        infoScope.Detail = angular.copy(contructedObject.Detail);
        var contentString = '<div><div ng-include="\'infoWindowUserClick\'"></div></div>';
        var compiledHtml = compile(contentString)(infoScope);


        contructedObject.MapMark.InfoWindow = new google.maps.InfoWindow();
        contructedObject.MapMark.InfoWindow.setContent(compiledHtml[0]);

        google.maps.event.clearListeners(contructedObject.MapMark, 'click');

        contructedObject.MapMark.InfoWindow.close();
        contructedObject.MapMark.setMap(this.CurrentMap);

        this.FixedMapPoints.push(contructedObject);

        google.maps.event.addListener(contructedObject.MapMark, 'click', function (e) {
            this.InfoWindow.open(this.map, this);
        });

        return contructedObject;
    }

    this.PanMapToFixedMapPoint = function (MarkToPan) {
        var index = this.FixedMapPoints.indexOf(MarkToPan);
        if (index >= 0) {
            this.CurrentMap.panTo(MarkToPan.MapMark.position);
        }
    }

    this.RemoveFixedMapPoint = function (MarkToPan) {
        var index = this.FixedMapPoints.indexOf(MarkToPan);
        if (index >= 0) {
            MarkToPan.MapMark.setMap(null);
            this.FixedMapPoints.splice(index, 1);
            this.CurrentMap.panTo(MarkToPan.MapMark.position);
        }
    }

});