﻿app = angular.module("AmigoRotaWebApp");



			app.controller('ddController', ['$scope', function ($scope) {
			    $scope.dropped = function (dragEl, dropEl) { // function referenced by the drop target
			        //this is application logic, for the demo we just want to color the grid squares
			        //the directive provides a native dom object, wrap with jqlite
			        var drop = angular.element(dropEl);
			        var drag = angular.element(dragEl);

			        //clear the previously applied color, if it exists
			        var bgClass = drop.attr('data-color');
			        if (bgClass) {
			            drop.removeClass(bgClass);
			        }

			        //add the dragged color
			        bgClass = drag.attr("data-color");
			        drop.addClass(bgClass);
			        drop.attr('data-color', bgClass);

			        //if element has been dragged from the grid, clear dragged color
			        if (drag.attr("x-lvl-drop-target")) {
			            drag.removeClass(bgClass);
			        }
			    }
			}]);

app.controller("PasseioController", ["$scope", "amigoRotaMapService", "amigoRotaService", "$compile", "$location", function ($scope, amigoRotaMapService, amigoRotaService, $compile, $location) {

    $scope.PinMapIcon = "/mapsPins/star-green.png";

    $scope.PasseiosParadas = amigoRotaService.PasseiosParadas;

    $scope.InitMap = function () {
        var storedItens = amigoRotaService.GetStorePasseiosParadasData();

        if (storedItens && storedItens.length > 0 && (amigoRotaService.PasseiosParadas.length == 0)) {
            var bounds = new google.maps.LatLngBounds();

            for (var i = 0; i < storedItens.length ; i++) {
                var item = amigoRotaMapService.RestoreFixedMapPoint(storedItens[i], $scope, $compile);
                bounds.extend(new google.maps.LatLng(item.location.lat, item.location.lng));
                $scope.mapItemAdded(item);
            }

            amigoRotaMapService.CurrentMap.fitBounds(bounds);
        } else {
            if (amigoRotaService.PasseiosParadas.length != 0) {
                var bounds = new google.maps.LatLngBounds();

                for (var i = 0; i < amigoRotaService.PasseiosParadas.length ; i++) {
                    
                    amigoRotaMapService.AddFixedMapPoints(amigoRotaService.PasseiosParadas[i], $scope, $compile);
                    amigoRotaService.PasseiosParadas[i].MapMark.setIcon($scope.PinMapIcon);

                    bounds.extend(new google.maps.LatLng(
                        amigoRotaService.PasseiosParadas[i].GeoCode[0].geometry.location.lat.call != undefined ? amigoRotaService.PasseiosParadas[i].GeoCode[0].geometry.location.lat() : amigoRotaService.PasseiosParadas[i].GeoCode[0].geometry.location.lat
                        , amigoRotaService.PasseiosParadas[i].GeoCode[0].geometry.location.lng.call != undefined ? amigoRotaService.PasseiosParadas[i].GeoCode[0].geometry.location.lng() : amigoRotaService.PasseiosParadas[i].GeoCode[0].geometry.location.lng));
                }
                try {
                    amigoRotaMapService.CurrentMap.fitBounds(bounds);
                } catch (err) {
                    var bounds = new google.maps.LatLngBounds();
                    bounds.extend(new google.maps.LatLng(amigoRotaService.PasseiosParadas[0].GeoCode[0].geometry.location.lat, amigoRotaService.PasseiosParadas[0].GeoCode[0].geometry.location.lng));
                    amigoRotaMapService.CurrentMap.fitBounds(bounds);

                }
            }

        }
    }

    $scope.reloadMapMarks = function () {
        amigoRotaMapService.FixedMapPoints = [];
        $scope.InitMap();
    }

    $scope.mapItemAdded = function (item) {
        item.Detail.Title = !item.Detail.Title || item.Detail.Title == "" ? "Novo Passeio/Parada" : item.Detail.Title;

        $scope.PasseiosParadas.push(item);

        amigoRotaMapService.AddFixedMapPoints(item, $scope, $compile);
        item.MapMark.setIcon($scope.PinMapIcon);

        amigoRotaService.StorePasseiosParadasData();
    }

    $scope.PanMapToMark = function (item) {
        amigoRotaMapService.PanMapToFixedMapPoint(item);
        amigoRotaMapService.UpdateFixedInfoWindow(item, $scope, $compile);
    }

    $scope.DeleteMapMark = function (item) {
        amigoRotaMapService.RemoveFixedMapPoint(item);

        var index = $scope.PasseiosParadas.indexOf(item);
        if (index >= 0) {
            $scope.PasseiosParadas.splice(index, 1);
            amigoRotaService.StorePasseiosParadasData();
        }
    }

    $scope.CollapseEdit = function (item, index) {
        item.Detail.currentIndex = index;

        $scope.$watch('PasseiosParadas[' + index + '].Detail', function (newValue, oldValue, scope) {
            if (newValue != undefined) {
                amigoRotaMapService.UpdateFixedInfoWindow(scope.PasseiosParadas[newValue.currentIndex], scope, $compile);
                amigoRotaService.StorePasseiosParadasData();
            }
        }, true);

        $scope.$watch('PasseiosParadas[' + index + '].Detail.Checkout', function (newValue, oldValue, scope) {
            if (newValue && newValue.value != "" && newValue.date) {
                //TODO: Incluir validação para calculo das noites                
            }
        }, true);

        $scope.$watch('PasseiosParadas[' + index + '].Detail.Nights', function (newValue, oldValue, scope) {
            if (newValue && newValue.value != "") {
                //TODO: Incluir validação para calculo das noites                
            }
        }, true);

        amigoRotaMapService.PanMapToFixedMapPoint(item);
    }


    $scope.GoToView = function () {
        $location.path("/Passeios");
    }


}]);