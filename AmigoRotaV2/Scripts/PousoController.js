﻿app = angular.module("AmigoRotaWebApp");

app.controller("PousoController", ["$scope", "amigoRotaMapService", "amigoRotaService", "$compile", "$location",function ($scope, amigoRotaMapService, amigoRotaService, $compile, $location) {
    
    var count = 0;

    $scope.PinMapIcon = "/mapsPins/lodging-green.png";
    
    $scope.PousosHoteis = amigoRotaService.PousosHoteis;

    $scope.InitMap = function () {
        var storedItens = amigoRotaService.GetStorePousosHoteisData();

        if (storedItens && storedItens.length > 0 && (amigoRotaService.PousosHoteis.length == 0)) {
            var bounds = new google.maps.LatLngBounds();

            for (var i = 0; i < storedItens.length ; i++) {
                var item = amigoRotaMapService.RestoreFixedMapPoint(storedItens[i], $scope, $compile);
                bounds.extend(new google.maps.LatLng(item.location.lat, item.location.lng));
                $scope.mapItemAdded(item);
            }

            amigoRotaMapService.CurrentMap.fitBounds(bounds);
        } else {
            if (amigoRotaService.PousosHoteis.length != 0) {
                var bounds = new google.maps.LatLngBounds();

                for (var i = 0; i < amigoRotaService.PousosHoteis.length ; i++) {
                    
                    amigoRotaMapService.AddFixedMapPoints(amigoRotaService.PousosHoteis[i], $scope, $compile);
                    amigoRotaService.PousosHoteis[i].MapMark.setIcon($scope.PinMapIcon);

                    bounds.extend(new google.maps.LatLng(
                        amigoRotaService.PousosHoteis[i].GeoCode[0].geometry.location.lat.call != undefined ? amigoRotaService.PousosHoteis[i].GeoCode[0].geometry.location.lat() : amigoRotaService.PousosHoteis[i].GeoCode[0].geometry.location.lat
                        , amigoRotaService.PousosHoteis[i].GeoCode[0].geometry.location.lng.call != undefined ? amigoRotaService.PousosHoteis[i].GeoCode[0].geometry.location.lng() : amigoRotaService.PousosHoteis[i].GeoCode[0].geometry.location.lng));
                }
                try {
                    amigoRotaMapService.CurrentMap.fitBounds(bounds);
                } catch (err) {
                    var bounds = new google.maps.LatLngBounds();
                    bounds.extend(new google.maps.LatLng(amigoRotaService.PousosHoteis[0].GeoCode[0].geometry.location.lat, amigoRotaService.PousosHoteis[0].GeoCode[0].geometry.location.lng));
                    amigoRotaMapService.CurrentMap.fitBounds(bounds);

                }
            }

        }
    }

    $scope.reloadMapMarks = function () {
        amigoRotaMapService.FixedMapPoints = [];
        $scope.InitMap();
      
    }
  
    $scope.mapItemAdded = function (item) {
        
        item.Detail.Title = !item.Detail.Title || item.Detail.Title == "" ? "Novo Pouso/Hotel" : item.Detail.Title;

        $scope.PousosHoteis.push(item);
                
        amigoRotaMapService.AddFixedMapPoints(item, $scope, $compile);
        item.MapMark.setIcon($scope.PinMapIcon);

        amigoRotaService.StorePousosHoteisData();
    }

    $scope.PanMapToMark = function (item) {
        amigoRotaMapService.PanMapToFixedMapPoint(item);
        amigoRotaMapService.UpdateFixedInfoWindow(item, $scope, $compile);
    }

    $scope.DeleteMapMark = function (item) {
        amigoRotaMapService.RemoveFixedMapPoint(item);
        
        var index = $scope.PousosHoteis.indexOf(item);
        if (index >= 0) {
            $scope.PousosHoteis.splice(index, 1);
            amigoRotaService.StorePousosHoteisData();
        }
    }

    $scope.CollapseEdit = function (item, index) {
        item.Detail.currentIndex = index;

        $scope.$watch('PousosHoteis[' + index + '].Detail', function (newValue, oldValue, scope) {
            if (newValue != undefined) {
                amigoRotaMapService.UpdateFixedInfoWindow(scope.PousosHoteis[newValue.currentIndex], scope, $compile);
                amigoRotaService.StorePousosHoteisData();
            }
        }, true);

        $scope.$watch('PousosHoteis[' + index + '].Detail.Checkout', function (newValue, oldValue, scope) {
            if (newValue && newValue.value != "" && newValue.date) {
                //TODO: Incluir validação para calculo das noites                
            }
        }, true);

        $scope.$watch('PousosHoteis[' + index + '].Detail.Nights', function (newValue, oldValue, scope) {
            if (newValue && newValue.value != "") {
                //TODO: Incluir validação para calculo das noites                
            }
        }, true);

        amigoRotaMapService.PanMapToFixedMapPoint(item);
    }

   
    $scope.GoToView = function() {
        $location.path("/Pousos");
    }

    
}]);