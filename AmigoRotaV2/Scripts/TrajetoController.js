﻿app = angular.module("AmigoRotaWebApp");

app.controller("TrajetoController", ["$scope", "amigoRotaMapService", "amigoRotaService", "$compile", "$location", "$rootScope", function ($scope, amigoRotaMapService, amigoRotaService, $compile, $location, $rootScope) {

    $scope.PasseiosParadas = amigoRotaService.PasseiosParadas;
    $scope.PousosHoteis = amigoRotaService.PousosHoteis;
	$scope.RouteDetails = $rootScope.Configuration.RouteDetails;
	$scope.arrayAvaliableDays = [];
	$scope.colors = ["bg-teal","bg-fuchsia","bg-olive","bg-light-blue","bg-purple","bg-lime", "bg-orange","bg-yellow"];
	
	$scope.TravelDates = function(){
		if($scope.arrayAvaliableDays.length == 0){
			var totalDays = $scope.RouteDetails.Nights;
			
			var lastTravelDate = angular.copy($scope.RouteDetails.Checkin.date);
			
			for(var ix =0; ix < totalDays; ix++){
				$scope.arrayAvaliableDays.push({
					CurrentDate: {
						date: angular.copy(lastTravelDate)
						, toString: function(){
							moment.locale('pt-br');
							return moment(this.date).format('DD MMM. YYYY');
						}
					}, Plan: null
				});
				lastTravelDate.setDate(lastTravelDate.getDate() + 1);
			}
		}
	}
	
	$scope.EditDate = function(currDate){
	    $scope.SelectedDate = currDate;
	    amigoRotaMapService.FixedMapPoints = [];

	}
	

	$scope.SetDayDeparture = function (dayItem, pousoItem) {
		if(dayItem.Departure){
			amigoRotaMapService.RemoveFixedMapPoint(dayItem.Departure);			
		}
		
		if(dayItem.Arrival == pousoItem){
			amigoRotaMapService.RemoveFixedMapPoint(dayItem.Arrival);
			dayItem.Arrival = null;
		}
		
		dayItem.Departure = pousoItem;
	    amigoRotaMapService.AddFixedMapPoints(pousoItem, $scope, $compile);
	}

	$scope.SetDayArrival = function (dayItem, pousoItem) {
	    if(dayItem.Arrival){
			amigoRotaMapService.RemoveFixedMapPoint(dayItem.Arrival);			
		}
		
		if(dayItem.Departure == pousoItem){
			amigoRotaMapService.RemoveFixedMapPoint(dayItem.Departure);
			dayItem.Departure = null;
		}
		
		dayItem.Arrival = pousoItem;
	    amigoRotaMapService.AddFixedMapPoints(pousoItem, $scope, $compile);
	}
	

	
	
	
	
    // $scope.InitMap = function () {
        // var storedItens = amigoRotaService.GetStorePasseiosParadasData();

        // if (storedItens && storedItens.length > 0 && (amigoRotaService.PasseiosParadas.length == 0)) {
            // var bounds = new google.maps.LatLngBounds();

            // for (var i = 0; i < storedItens.length ; i++) {
                // var item = amigoRotaMapService.RestoreFixedMapPoint(storedItens[i], $scope, $compile);
                // bounds.extend(new google.maps.LatLng(item.location.lat, item.location.lng));
                // $scope.mapItemAdded(item);
            // }

            // amigoRotaMapService.CurrentMap.fitBounds(bounds);
        // } else {
            // if (amigoRotaService.PasseiosParadas.length != 0) {
                // var bounds = new google.maps.LatLngBounds();

                // for (var i = 0; i < amigoRotaService.PasseiosParadas.length ; i++) {
                    
                    // amigoRotaMapService.AddFixedMapPoints(amigoRotaService.PasseiosParadas[i], $scope, $compile);
                    // amigoRotaService.PasseiosParadas[i].MapMark.setIcon($scope.PinMapIcon);

                    // bounds.extend(new google.maps.LatLng(
                        // amigoRotaService.PasseiosParadas[i].GeoCode[0].geometry.location.lat.call != undefined ? amigoRotaService.PasseiosParadas[i].GeoCode[0].geometry.location.lat() : amigoRotaService.PasseiosParadas[i].GeoCode[0].geometry.location.lat
                        // , amigoRotaService.PasseiosParadas[i].GeoCode[0].geometry.location.lng.call != undefined ? amigoRotaService.PasseiosParadas[i].GeoCode[0].geometry.location.lng() : amigoRotaService.PasseiosParadas[i].GeoCode[0].geometry.location.lng));
                // }
                // try {
                    // amigoRotaMapService.CurrentMap.fitBounds(bounds);
                // } catch (err) {
                    // var bounds = new google.maps.LatLngBounds();
                    // bounds.extend(new google.maps.LatLng(amigoRotaService.PasseiosParadas[0].GeoCode[0].geometry.location.lat, amigoRotaService.PasseiosParadas[0].GeoCode[0].geometry.location.lng));
                    // amigoRotaMapService.CurrentMap.fitBounds(bounds);

                // }
            // }

        // }
    // }

    $scope.reloadMapMarks = function () {
        // amigoRotaMapService.FixedMapPoints = [];
        // $scope.InitMap();
    }

    $scope.mapItemAdded = function (item) {
        // item.Detail.Title = !item.Detail.Title || item.Detail.Title == "" ? "Novo Passeio/Parada" : item.Detail.Title;

        // $scope.PasseiosParadas.push(item);

        // amigoRotaMapService.AddFixedMapPoints(item, $scope, $compile);
        // item.MapMark.setIcon($scope.PinMapIcon);

        // amigoRotaService.StorePasseiosParadasData();
    }
	
	
	
	
	
	
	
	
	
	
	

    $scope.ItemSelected = function (item) {
		amigoRotaMapService.AddFixedMapPoints(item, $scope, $compile)
		amigoRotaMapService.PanMapToFixedMapPoint(item);
    }
	
	$scope.$watch('RouteDetails', function(newValue, oldValue){
		if($scope.arrayAvaliableDays.length > 0){

			if(newValue.Checkin.date.toISOString().split('T')[0] != $scope.arrayAvaliableDays[0].CurrentDate.date.toISOString().split('T')[0] ){
				
				var lastTravelDate = angular.copy(newValue.Checkin.date);
				
				for(var ix=0; u=ix < $scope.arrayAvaliableDays.length; ix++){
					$scope.arrayAvaliableDays[ix].CurrentDate = {
						date: angular.copy(lastTravelDate)
						, toString: function(){
							moment.locale('pt-br');
							return moment(this.date).format('DD MMM. YYYY');
						}
					};
					lastTravelDate.setDate(lastTravelDate.getDate() + 1);
				}			
				
			}		
			
			if($scope.arrayAvaliableDays.length > parseInt(newValue.Nights)){
				$scope.arrayAvaliableDays = $scope.arrayAvaliableDays.slice(0, parseInt(newValue.Nights));
			} else if($scope.arrayAvaliableDays.length < parseInt(newValue.Nights)) {
				
				var lastTravelDate = angular.copy($scope.arrayAvaliableDays[$scope.arrayAvaliableDays.length-1].CurrentDate.date);
				lastTravelDate.setDate(lastTravelDate.getDate() + 1);
				
				for(var ix= $scope.arrayAvaliableDays.length; ix <  parseInt(newValue.Nights); ix++){
					$scope.arrayAvaliableDays.push({
						CurrentDate: {
							date: angular.copy(lastTravelDate)
							, toString: function(){
								moment.locale('pt-br');
								return moment(this.date).format('DD MMM. YYYY');
							}
						}, Plan: null
					});
					lastTravelDate.setDate(lastTravelDate.getDate() + 1);
				}				
			}
			
		}else if(newValue && newValue.Checkin){
			$scope.TravelDates();			
		}
	}, true);

}]);